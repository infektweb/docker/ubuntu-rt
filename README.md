# ubuntu-rt

Ubuntu runtime image, adds essentials to base ubuntu image like:

  - ca-certificates

Has an `/app` folder and sets the workdir to it.