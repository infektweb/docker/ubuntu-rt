FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update \
    && apt -y upgrade \
    && apt -y install ca-certificates \
    && rm -rf /var/lib/apt/lists/*
 

RUN mkdir /app
WORKDIR /app

